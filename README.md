# Eagle's Translation

This repository contains configuration files of translated messages in some plugins.

You can contribute and fix mistakes in translated messages!

## Style

Please try to copy this style, when you're translating some messages:

```yaml
Prefix | This is a message.
```

Don't change style to something like this:
```yaml
# Wrong style
[Prefix] This is a message.
|Prefix| This is a message.
(Prefix) This is a message.
Prefix This is a message.
Prefix - This is a message.
```

## Colors

Don't use hard-reading colors like:
- ![](https://placehold.co/15x15/AA0000/AA0000.png) `&4`
- ![](https://placehold.co/15x15/00AA00/00AA00.png) `&2`
- ![](https://placehold.co/15x15/0000AA/0000AA.png) `&1`
- ![](https://placehold.co/15x15/00000/00000.png) `&0`

Instead, use this colors:

- ![](https://placehold.co/15x15/FFAA00/FFAA00.png) `&6`
- ![](https://placehold.co/15x15/555555/555555.png) `&8`
- ![](https://placehold.co/15x15/AAAAAA/AAAAAA.png) `&7`
- ![](https://placehold.co/15x15/FFFFFF/FFFFFF.png) `&f`
- ![](https://placehold.co/15x15/55FF55/55FF55.png) `&a`
- ![](https://placehold.co/15x15/FF5555/FF5555.png) `&c`

Don't make messages bold, italic or underscored if it's not really required.
